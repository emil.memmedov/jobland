<?php


namespace App\Traits;
use App\Models\Vacation;

trait ClientHelper
{
    public function checkJobs($client){
        $vacations = Vacation::where('category_id',$client->category_id);
        if($vacations->count() !== 0) {
            if (!is_null($client->min_salary)) {
                $vacations = $vacations->where('salary', '>=', $client->min_salary);
            }

            if (!is_null($client->max_salary)) {
                $vacations = $vacations->where('salary', '<=', $client->max_salary);
            }
            if (!is_null($client->age)) {
                $vacations = $vacations->where(function ($query) use ($client) {
                    $query->where('min_age', '<', $client->age);
                    $query->orWhere('min_age', null);
                });
                $vacations = $vacations->where(function ($query) use ($client) {
                    $query->where('max_age', '>', $client->age);
                    $query->orWhere('max_age', null);
                });
            }
            if(!is_null($client->experience)){
                $vacations = $vacations->where('experience', $client->experience);
            }
            $vacations = $vacations->get();
        }
        return $vacations;
    }
}
