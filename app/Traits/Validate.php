<?php


namespace App\Traits;


use Illuminate\Support\Facades\Validator;

trait Validate
{
    private $validationTypes = [
        'loginuser'=>1,
        'loginclient'=>2,
        'createuser'=>3,
        'createclient'=>4,
        'createjob'=>5,
        'addcomment'=>6
    ];

    public function Validation($request,$type){
        if($type === $this->validationTypes['createuser']){
            $validator = Validator::make($request->all(),[
                'name'=>'required',
                'email'=>'required|email|unique:users',
                'company_name'=>'required|unique:users',
                'password'=>'required|max:20|min:6',
                'type'=>'required'
            ]);
            if($validator->fails()){
                return [false,$validator->errors()];
            }
            else{
                return [true];
            }
        }
        else if($type === $this->validationTypes['createclient']){
            $validator = Validator::make($request->all(),[
                'name'=>'required',
                'email'=>'required|email|unique:clients',
                'age'=>'required',
                'phone_number'=>'required',
                'category_id'=>'required',
                'experience'=>'required',
                'password'=>'required|max:20|min:6'
            ]);
            if($validator->fails()){
                return [false,$validator->errors()];
            }
            else{
                return [true];
            }
        }
        else if($type === $this->validationTypes['loginuser']){
            $validator = Validator::make($request->all(),[
                'email'=>'required|email',
                'password'=>'required|max:20|min:6',
            ]);
            if($validator->fails()){
                return [false,$validator->errors()];
            }
            else{
                return [true];
            }
        }
        else if($type === $this->validationTypes['loginclient']){
            $validator = Validator::make($request->all(),[
                'email'=>'required|email',
                'password'=>'required|max:20|min:6',
            ]);
            if($validator->fails()){
                return [false,$validator->errors()];
            }
            else{
                return [true];
            }
        }
        else if($type === $this->validationTypes['createjob']){
            $validator = Validator::make($request->all(),[
                'category_id'=>'required',
                'title'=>'required',
                'info'=>'required',
                'phoneNumber'=>'required',
            ]);
            if($validator->fails()){
                return [false,$validator->errors()];
            }
            else{
                return [true];
            }
        }
        else if($type === $this->validationTypes['addcomment']){
            $validator = Validator::make($request->all(),[
                'body'=>'required',
                'vacation_id'=>'required'
            ]);
            if($validator->fails()){
                return [false,$validator->errors()];
            }
            else{
                return [true];
            }
        }
    }
}
