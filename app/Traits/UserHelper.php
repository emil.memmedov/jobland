<?php

namespace App\Traits;

use App\Models\Client;

trait UserHelper
{
    private function checkWorkers($vacation){
        $clients = Client::where('category_id',$vacation->category_id)->get();
        if(count($clients) !== 0) {
            if (!is_null($vacation->min_age)) {
                $clients = $clients->where('age', '>', $vacation->min_age);
            }
            if (!is_null($vacation->max_age)) {
                $clients = $clients->where('age', '<', $vacation->max_age);
            }
            if (!is_null($vacation->salary)) {
                $clients = $clients->where('min_salary', '<=', $vacation->salary);
                $clients = $clients->where('max_salary', '>', $vacation->salary);
            }
            if(!is_null($vacation->experience)){
                $clients = $clients->where('experience', $vacation->experience);
            }
        }
        return $clients;
    }
}
