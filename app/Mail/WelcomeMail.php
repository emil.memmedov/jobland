<?php

namespace App\Mail;

use App\Models\Category;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class WelcomeMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $vacation;
    public function __construct($vacation)
    {
        $this->vacation = $vacation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from('emill.memmedovv@gmail.com')
            ->subject('Job Detail')
            ->view('Mail')
            ->with([
                'vacation'=>$this->vacation,
                'category'=>Category::where('id',$this->vacation->category_id)->get('title')[0]->title
            ]);
    }
}
