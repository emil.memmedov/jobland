<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendInfo extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $vacations;
    public function __construct($vacations)
    {
        $this->vacations = $vacations;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->view('Info')
            ->subject('New Vacations for You')
            ->from('emill.memmedovv@gmail.com')
            ->with([
                'vacations'=>$this->vacations
            ]);
    }
}
