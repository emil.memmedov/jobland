<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Client extends Authenticatable implements JWTSubject
{
    protected $guard = 'clients';
    protected $fillable = [
        'name',
        'email',
        'password',
        'age',
        'phone_number',
        'category_id',
        'experience',
        'min_salary',
        'max_salary'
    ];
    protected $hidden = [
        'email_verified_at','password','remember_token','created_at','updated_at'
    ];
    use HasFactory;

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
}
