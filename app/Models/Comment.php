<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;
    protected $hidden = ['id','updated_at'];
    protected $fillable = ['body','client_id','vacation_id',];
    public function getCreatedAtAttribute($value){
        return date('d:m H:i',strtotime($value));
    }
}
