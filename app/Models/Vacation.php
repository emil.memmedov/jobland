<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vacation extends Model
{
    protected $fillable = [
        'category_id',
        'title',
        'info',
        'phoneNumber',
        'user_id',
        'salary',
        'min_age',
        'max_age',
        'experience'
    ];
    protected $hidden = [
        'created_at','updated_at','pivot'
    ];
    use HasFactory;
    public function users(){
        return $this->belongsToMany(User::class,'user_job','vacation_id','user_id');
    }
    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function comments(){
        return $this->hasMany(Comment::class);
    }
}
