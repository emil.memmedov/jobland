<?php

namespace App\Listeners;

use App\Events\SendWelcomeEvent;
use App\Mail\WelcomeMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class SendWelcomeListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendWelcomeEvent  $event
     * @return void
     */
    public function handle(SendWelcomeEvent $event)
    {
        Mail::to(Auth::guard('users')->user()->email)->send(new WelcomeMail($event->vacation));
    }
}
