<?php

namespace App\Jobs;

use App\Mail\SendInfo;
use App\Traits\ClientHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendInfoEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use ClientHelper;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $client;
    public function __construct($client)
    {
        $this->client = $client;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $vacations = $this->checkJobs($this->client);
        if(!empty($vacations->first())){
            Mail::to($this->client->getAttribute('email'))->send(new SendInfo($vacations));
        }
    }
}
