<?php

namespace App\Console\Commands;
use App\Jobs\SendInfoEmailJob;
use App\Models\Client;
use Illuminate\Console\Command;

class SendEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $clients = Client::all();
        foreach ($clients as $client){
            /*dispatch(new SendInfoEmailJob($client));*/
            SendInfoEmailJob::dispatch($client);
        }
    }
}
