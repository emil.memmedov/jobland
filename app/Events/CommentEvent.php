<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CommentEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $body;
    public $vacationId;
    public $client;
    public function __construct($body,$vacationId,$client)
    {
        $this->body = $body;
        $this->vacationId = $vacationId;
        $this->client = $client;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn():object
    {
        return new Channel('client-comment');
    }
    public function broadcastAs():string{
	    return 'comment';
    }

    public function broadcastWith():array
    {
	    return [
		    'body'=>$this->body,
            'vacationId'=>$this->vacationId,
            'client'=>$this->client
        ];
    }
}

