<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class checkAuthForClient
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        if(is_null(Auth::guard('clients')->user())){
            return response()->json([
                'error'=>'Not user authenticated',
                'status'=>'401'
            ],401);
        }
        return $next($request);
    }
}
