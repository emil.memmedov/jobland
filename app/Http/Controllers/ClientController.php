<?php

namespace App\Http\Controllers;

use App\Events\CommentEvent;
use App\Models\Category;
use App\Models\Client;
use App\Models\Comment;
use App\Models\Vacation;
use App\Traits\ClientHelper;
use App\Traits\Validate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ClientController extends Controller
{
    use ClientHelper,Validate;
    public function getJobs($id){
        $vacations = Vacation::where('category_id',$id)->get();
        $data = [];
        $index = 0;
        foreach ($vacations as $vacation){
            array_push($data,$vacation);
            if(!is_null(Vacation::find($vacation->id)->comments()->latest()->first())){
                $data[$index]['last_comment'] = Vacation::find($vacation->id)->comments()->latest()->first();
                $data[$index]['last_comment_time'] = Vacation::find($vacation->id)->comments()->latest()->first()->created_at;
            }
            $data[$index]['comments_count'] = Vacation::find($vacation->id)->comments()->count();
            $index++;
        }
        return response()->json([
            'vacations'=>$data,
        ]);
    }
    public function getCategories(){
        return response()->json([
            'categories'=>Category::all()
        ]);
    }
    public function getInfoAboutClient(){
        return response()->json([
            'information'=>Auth::guard('clients')->user()
        ]);
    }
    public function editData(Request $request){
        $client = Auth::guard('clients')->user();
        $client->update($request->all());
        return response()->json([
            'message'=>'Your data updated successfully',
            'data'=>$client
        ]);
    }
    public function getSpecialJobs(){
        $client = Auth::guard('clients')->user();
        $data = $this->CheckJobs($client);
        return response()->json(['data'=>$data]);
    }
    public function addComment(Request $request){
        $validated = $this->Validation($request, $this->validationTypes['addcomment']);
        if(!$validated[0]){
            return response()->json([
                'error'=>$validated[1]
            ],422);
        }
        $comment = new Comment();
        $comment->setAttribute('body',$request->get('body'));
        $comment->setAttribute('vacation_id',$request->get('vacation_id'));
        $comment->setAttribute('client_id',Auth::guard('clients')->user()->id);
        $comment->save();

        event(new CommentEvent(
            $comment->getAttribute('body'),
            $comment->getAttribute('vacation_id'),
            Client::find($comment->getAttribute('client_id'))->email));
//        broadcast(new CommentEvent());

        return response()->json([
            'message'=>'Comment added successfully',
            'comment'=>$comment
        ]);
    }
    public function showComments($id){
        $comments = Vacation::find($id)->comments()->get();
        $data = [];
        foreach ($comments as $comment){
            array_push($data,[
                'client_id'=>Client::find($comment->client_id)->id,
                'client_email'=>Client::find($comment->client_id)->email,
                'body'=>$comment->body,
                'time'=>$comment->created_at,
            ]);
        }
        return response()->json([
            'comments'=>$data
        ]);
    }
}
