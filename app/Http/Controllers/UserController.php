<?php

namespace App\Http\Controllers;

use App\Events\SendWelcomeEvent;
use App\Models\Vacation;
use App\Traits\UserHelper;
use App\Traits\Validate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    use UserHelper, Validate;

    public function createJob(Request $request){
        $validated = $this->Validation($request, $this->validationTypes['createjob']);
        if(!$validated[0]){
            return response()->json([
                'error'=>$validated[1]
            ],422);
        }
        else{
            $vacation = Vacation::create([
                'category_id' => $request->get('category_id'),
                'title'=>$request->get('title'),
                'info'=>$request->get('info'),
                'phoneNumber'=>$request->get('phoneNumber'),
                'min_age'=>$request->get('min_age'),
                'max_age'=>$request->get('max_age'),
                'experience'=>$request->get('experience'),
                'salary'=>$request->get('salary')
            ]);
            DB::insert('insert into user_job (user_id, vacation_id) values (?,?)',[Auth::guard('users')->user()->id, $vacation->id]);
            event(new SendWelcomeEvent($vacation));
            return response()->json([
                'message'=>'Vacation Created Successfully',
                'vacation'=>$vacation
            ]);
        }
    }
    public function getUserJobs(){
        return response()->json([
            'vacations'=> Auth::guard('users')->user()->vacations()->get()
        ]);
    }
    public function getSpecialWorkers($id){
        $vacation = Auth::guard('users')->user()->vacations()->where('vacations.id',$id)->get();
        $clients = $this->checkWorkers($vacation[0]);
        return response()->json([
            'clients'=>$clients
        ]);
    }
    public function editData(Request $request,$id){
        $vacation = Auth::guard('users')->user()->vacations()->find($id);
        $vacation->update($request->all());
        return response()->json([
            'message'=>'Your data updated successfully',
            'vacation'=>$vacation
        ]);
    }
}
