<?php

namespace App\Http\Controllers;

use App\Events\SendWelcomeEvent;
use App\Models\Client;
use App\Models\User;
use App\Traits\Validate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['logoutClient','logoutUser','loginUser','loginClient','createUser','createClient', 'verify','resend']]);
    }
    use Validate;
    public function createUser(Request $request){
        $validated = $this->Validation($request, $this->validationTypes['createuser']);
        if(!$validated[0]){
            return response()->json([
                'error'=>$validated[1]
            ],422);
        }
        else{
            User::create([
                'name'=>$request->name,
                'email'=>$request->email,
                'company_name'=>$request->company_name,
                'password'=>bcrypt($request->password)
            ])->sendEmailVerificationNotification();
        }
        return response()->json([
            'message'=>'User created Succesfully',
            'verified'=>false
        ]);
    }
    public function createClient(Request $request){
        $validated = $this->Validation($request, $this->validationTypes['createclient']);
        if(!$validated[0]){
            return response()->json([
                'error'=>$validated[1]
            ],422);
        }
        else{
            $client = Client::create([
                'name'=>$request->get('name'),
                'email'=>$request->get('email'),
                'age'=>$request->get('age'),
                'phone_number'=>$request->get('phone_number'),
                'category_id'=>$request->get('category_id'),
                'experience'=>$request->get('experience'),
                'min_salary'=>$request->get('min_salary'),
                'max_salary'=>$request->get('max_salary'),
                'password'=>bcrypt($request->password),
            ]);
        }
        return response()->json([
            'message'=>'Client created Succesfully',
            'verified'=>false
        ]);
    }
    public function verify(Request $request, $user_id){
        if (!$request->hasValidSignature()) {
            return response()->json(["msg" => "Invalid/Expired url provided."], 401);
        }
        $user = $request->type === 1?User::findOrFail($user_id):Client::findOrFail($user_id);

        if (!$user->hasVerifiedEmail()) {
            $user->markEmailAsVerified();
        }

        return response()->json([
            'message'=>'email verified'
        ]);
    }
    public function resend() {
        if (auth()->user()->hasVerifiedEmail()) {
            return response()->json(["msg" => "Email already verified."], 400);
        }

        auth()->user()->sendEmailVerificationNotification();

        return response()->json(["msg" => "Email verification link sent on your email id"]);
    }
    public function loginUser(Request $request){
        $validated = $this->Validation($request, $this->validationTypes['loginuser']);
        if(!$validated[0]){
            return response()->json([
                'error'=>$validated[1]
            ],422);
        }
        $credentials = request(['email', 'password']);
        if (! $token = Auth::guard('users')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return response()->json([
            'user'=> Auth::guard('users')->user(),
            'token'=>$token
        ]);
    }
    public function loginClient(Request $request){
        $validated = $this->Validation($request, $this->validationTypes['loginclient']);
        if(!$validated[0]){
            return response()->json([
                'error'=>$validated[1]
            ],422);
        }
        $credentials = request(['email', 'password']);
        if (! $token = Auth::guard('clients')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return response()->json([
            'user'=> Auth::guard('clients')->user(),
            'token'=>$token
        ]);
    }
    public function logoutUser(){
        Auth::guard('users')->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }
    public function logoutClient(){
        Auth::guard('clients')->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }
}
