<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        DB::table('categories')->insert([
            [
                'id'=>1,
                'title'=>'IT',
            ],
            [
                'id'=>2,
                'title'=>'Education',
            ],
            [
                'id'=>3,
                'title'=>'Marketing',
            ],
            [
                'id'=>4,
                'title'=>'Design',
            ],
            [
                'id'=>5,
                'title'=>'Lawyer',
            ],
            [
                'id'=>6,
                'title'=>'Saler',
            ],
            [
                'id'=>7,
                'title'=>'Security Guard',
            ],
        ]);

        DB::table('users')->insert([
            'name'=>'Emil',
            'email'=>'emill.memmedovv@gmail.com',
            'company_Name'=>'Private Business',
            'password'=>'eemil123456'
        ]);
        DB::table('clients')->insert([
            [
                'name'=>'Rena',
                'email'=>'rrena.123456.rrena@gmail.com',
                'password'=>bcrypt('rrena123456'),
                'age'=>25,
                'category_id'=>2,
                'phone_number'=>'+994504567518',
                'min_salary'=>300,
                'max_salary'=>1000,
                'experience'=>0
            ],
            [
                'name'=>'Ilham',
                'email'=>'iilham.123456.iilham@gmail.com',
                'password'=>bcrypt('iilham123456'),
                'age'=>25,
                'category_id'=>1,
                'phone_number'=>'+994504567518',
                'min_salary'=>500,
                'max_salary'=>1400,
                'experience'=>0
            ],
            [
                'name'=>'Namiq',
                'email'=>'nnamiq.123456.nnamiq@gmail.com',
                'password'=>bcrypt('nnamiq123456'),
                'age'=>26,
                'category_id'=>4,
                'phone_number'=>'+994504567518',
                'min_salary'=>400,
                'max_salary'=>800,
                'experience'=>0
            ],
            [
                'name'=>'Sebine',
                'email'=>'ssebine.123456.ssebine@gmail.com',
                'password'=>bcrypt('ssebine123456'),
                'age'=>28,
                'category_id'=>4,
                'phone_number'=>'+994504567518',
                'min_salary'=>800,
                'max_salary'=>1600,
                'experience'=>1
            ],
        ]);
        DB::table('vacations')->insert([
            [
                'category_id'=>1,
                'title'=>'Network Administrator',
                'info'=>'Cisco,Comptia A+, Active Directory',
                'phoneNumber'=>'+994557892496',
                'salary'=>1100,
                'experience'=>1
            ],
            [
                'category_id'=>2,
                'title'=>'English teacher',
                'info'=>'IELTS 6+',
                'phoneNumber'=>'+994557892496',
                'salary'=>600,
                'experience'=>0
            ],
            [
                'category_id'=>4,
                'title'=>'UI UX designer',
                'info'=>'Figma, Photoshop',
                'phoneNumber'=>'+994557892496',
                'salary'=>600,
                'experience'=>0
            ],
            [
                'category_id'=>4,
                'title'=>'Graphic designer',
                'info'=>'Adobe Affect 2016',
                'phoneNumber'=>'+994557892496',
                'salary'=>750,
                'experience'=>1
            ],
            [
                'category_id'=>4,
                'title'=>'Editor',
                'info'=>'Adobe Sound Effect, DJ Max',
                'phoneNumber'=>'+994557892496',
                'salary'=>900,
                'experience'=>1
            ],
            [
                'category_id'=>4,
                'title'=>'Home Designer',
                'info'=>'3d Home, 3d Max',
                'phoneNumber'=>'+994557892496',
                'salary'=>1000,
                'experience'=>0
            ],
        ]);
        DB::table('user_job')->insert([
            [
                'user_id'=>1,
                'vacation_id'=>1
            ],
            [
                'user_id'=>1,
                'vacation_id'=>2
            ],
            [
                'user_id'=>1,
                'vacation_id'=>3
            ],
            [
                'user_id'=>1,
                'vacation_id'=>4
            ],
            [
                'user_id'=>1,
                'vacation_id'=>5
            ],
            [
                'user_id'=>1,
                'vacation_id'=>6
            ],
        ]);
    }
}
