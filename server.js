var app = require('express');
var server = require('http').Server(app);
var io = require('socket.io')(server,{
cors: {
    origin: "http://localhost:4200",
    methods: ["GET", "POST"]
  }
});

io.on('connection',function(socket){
	console.log('A user connected');
	socket.on('comment',(data)=>{
		console.log(data);
		socket.broadcast.emit('comment', data);
	})
	

});

server.listen(6001,()=>{
	console.log('Socket server is listening on port 6001');
})
