<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('createuser', [\App\Http\Controllers\AuthController::class, 'createUser']);
Route::post('createclient', [\App\Http\Controllers\AuthController::class, 'createClient']);
Route::post('loginasuser', [\App\Http\Controllers\AuthController::class, 'loginUser']);
Route::post('loginasclient', [\App\Http\Controllers\AuthController::class, 'loginClient']);
Route::post('refresh', [\App\Http\Controllers\AuthController::class, 'refresh']);

Route::get('email/verify/{id}', [\App\Http\Controllers\AuthController::class, 'verify'])
    ->name('verification.verify');
Route::get('email/resend', [\App\Http\Controllers\AuthController::class, 'resend'])
    ->name('verification.resend');

Route::middleware('authforuser')->group(function (){
    Route::post('logoutuser', [\App\Http\Controllers\AuthController::class, 'logoutUser']);
    Route::post('createjob', [\App\Http\Controllers\UserController::class, 'createJob']);
    Route::get('getuserjobs', [\App\Http\Controllers\UserController::class, 'getUserJobs']);
    Route::get('getspecialworkers/{id}', [\App\Http\Controllers\UserController::class, 'getSpecialWorkers']);
    Route::get('editjobdata/{id}', [\App\Http\Controllers\UserController::class, 'editData']);
});
Route::middleware('authforclient')->group(function (){
    Route::post('logoutclient', [\App\Http\Controllers\AuthController::class, 'logoutClient']);
    Route::get('getinfoaboutclient', [\App\Http\Controllers\ClientController::class, 'getInfoAboutClient']);
    Route::get('editclientdata', [\App\Http\Controllers\ClientController::class, 'editData']);
    Route::get('getspecialjobs', [\App\Http\Controllers\ClientController::class, 'getSpecialJobs']);
    Route::post('addcomment', [\App\Http\Controllers\ClientController::class, 'addComment']);
});

Route::get('getjobs/{id}', [\App\Http\Controllers\ClientController::class, 'getJobs']);
Route::get('getcategories', [\App\Http\Controllers\ClientController::class, 'getCategories']);
Route::get('showcomments/{id}', [\App\Http\Controllers\ClientController::class, 'showComments']);
